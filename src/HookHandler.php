<?php

namespace Drupal\eca_flag;

use Drupal\eca\Event\BaseHookHandler;
use Drupal\flag\FlaggingInterface;

/**
 * The handler for hooks within the eca_flag.module file.
 *
 * @internal
 *   This class is not meant to be used as a public API. It is subject for name
 *   change or may be removed completely, also on minor version updates.
 */
class HookHandler extends BaseHookHandler {

  /**
   * Dispatches event insert.
   *
   * @param \Drupal\flag\FlaggingInterface $entity
   *   The entity.
   */
  public function insert(FlaggingInterface $entity): void {
    $this->triggerEvent->dispatchFromPlugin('flag:insert', $entity);
  }

  /**
   * Dispatches event delete.
   *
   * @param \Drupal\flag\FlaggingInterface $entity
   *   The entity.
   */
  public function delete(FlaggingInterface $entity): void {
    $this->triggerEvent->dispatchFromPlugin('flag:delete', $entity);
  }

}
