<?php

namespace Drupal\eca_flag\Event;

/**
 * Provides an event when a flagging entity is being inserted.
 *
 * @internal
 *   This class is not meant to be used as a public API. It is subject for name
 *   change or may be removed completely, also on minor version updates.
 *
 * @package Drupal\eca_flag\Event
 */
class FlaggingInsert extends FlaggingBase {

}
